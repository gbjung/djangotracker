from django.contrib import admin
from . import models

# Register your models here.
admin.site.register(models.List)
admin.site.register(models.Entry)
admin.site.register(models.Comment)
admin.site.register(models.Vote)
