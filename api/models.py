from __future__ import unicode_literals
import datetime
from django.core.urlresolvers import reverse
from django.contrib.auth.models import User
from django.db import models
from website.models import SlackAuth
import random

VOTE_CHOICES = (
	("UPVOTE", "Upvote"),
	("DOWNVOTE", "Downvote"),
)

COLORS = ["D4E7F5", "7DA5C3", "1E1936", "42759C", "FEAFBC",
		  "78D2D6", "EA5652", "f8baac", "1842AB", "C663B4"]

# Create your models here.
class List(models.Model):
	name = models.CharField(max_length=255, unique=True)
	created_date = models.DateTimeField(auto_now_add=True)
	creator = models.ForeignKey(SlackAuth)

	def get_absolute_url(self):
		return reverse('website:listdetail', kwargs={'pk':self.pk})

	def getEntries(self):
		return Entry.objects.filter(parent=self)

	def randomColor(self):
		return random.choice(COLORS)

	def __str__(self):
		return self.name

class Entry(models.Model):
	parent = models.ForeignKey(List, on_delete=models.CASCADE, related_name="entries")
	entrydescription = models.TextField()
	created_at = models.DateTimeField(auto_now_add=True)
	username = models.CharField(max_length=255)

	def get_absolute_url(self):
		return reverse('website:entrydetail', kwargs={'pk':self.pk})

	def getComments(self):
		return Comment.objects.filter(parent=self).count()

	def getVotes(self):
		return Vote.objects.filter(parententry=self)

	def upVotes(self):
		return Vote.objects.filter(parententry=self,vote="UPVOTE").count()

	def downVotes(self):
		return Vote.objects.filter(parententry=self,vote="DOWNVOTE").count()

	def getUser(self):
		return SlackAuth.objects.filter(name=self.username)[0]

	def __str__(self):
		return "{}: {}".format(self.parent, self.entrydescription)

class Comment(models.Model):
	parent = models.ForeignKey(Entry, on_delete=models.CASCADE, related_name="comments")
	comment = models.TextField()
	created_date = models.DateTimeField(auto_now_add=True)
	name = models.CharField(max_length=255, blank=True)

class Vote(models.Model):
	user = models.ForeignKey(SlackAuth)
	parententry = models.ForeignKey(Entry)
	vote = models.CharField(choices=VOTE_CHOICES, max_length=8)

	class Meta:
		unique_together = ('user','parententry')

	def __str__(self):
		return "{} voted {}".format(self.user.name, self.vote)
