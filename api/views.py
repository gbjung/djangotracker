from django.shortcuts import get_object_or_404
from django.views.decorators.csrf import csrf_exempt
from django.http import JsonResponse,HttpResponse, HttpResponseRedirect
from django.views import View
from django.shortcuts import redirect, render
from django.conf import settings
from django.core.serializers import serialize
from rest_framework import generics, mixins, viewsets
from rest_framework.decorators import detail_route, permission_classes
from rest_framework.response import Response
from rest_framework.views import APIView
from rest_framework.permissions import IsAuthenticated
from slackclient import SlackClient
import json
import random
import datetime
import requests
from . import models
from . import serializers




'''
API and Slack stuff
api/v2/lists/
api/v2/lists/<list pk>/		    <--- queries for all entries from specified list
api/v2/entries/<entry pk> 		<--- queries for all comments from specified entry
api/v2/comments/<entry pk>		<--- queries for comment based its primary key
can GET, POST, PUT, and delete
'''

#test method
class test(generics.ListCreateAPIView):
	queryset = models.List.objects.all() #required field for this generic
	ListIndex = {} #holds all the lists and their PK's
	for query in queryset:
		ListIndex[str(query.name)] = query.id
	#base response in Slack's format 
	base_response = {"response_type": "in_channel","mrkdwn":True}
	#attachment to add onto base response. Base response doesn't require
	#an attachment however
	attachments = {"callback_id": "list","color": "#3AA3E3",
					"attachment_type": "default", "mrkdwn_in": ["text", "pretext"] }
	#shouldn't really allow get requests since slack slash commands only post
	def get(self, request):
		return HttpResponse("Nope")
	#Main function that handles the various slack commands. Each routes to a different
	#function that returns the formatted JSON to be sent back to Slack
	def post(self, request, format=None):
		attachments = self.attachments
		base = self.base_response
		attachments['fields'] = ""
		base['text'] = ""
		attachments['image_url'] = ""
		command = request.data["text"].strip()
		if request.data["text"] == "hi":
			return Response(base)
		if "comment:" in command:
			return Response(self.AddComment(request,base,attachments))

		#command for adding a new entry into a list
		if "add:" in command:
			return Response(self.AddEntry(request, base, attachments))

		#command for creating a new list
		if "new:" in command:
			return Response(self.NewList(request, base, attachments))

		#command for showing list detail
		if "show:" in command:
			name = " ".join(request.data["text"].split(":")[1:]).strip()
			return Response(self.ListDetails(base, attachments, name))
		#command for displaying all lists
		if command == "lists":
			return Response(self.DisplayLists(base, attachments))

		#command for removing lists
		if "remove: " in command:
			return Response(self.RemoveList(request, base, attachments))

		if "love" in command:
			return Response(self.LoveResponse(base, attachments))

		else:
			return Response(self.CommandNotRecognized(base, attachments))

	def ReturnLists(self):
		'''
		queries for all List objects and returns their name and creation date
		in a nested list to be used as a field in a base message
		'''
		queryset = self.queryset
		data = self.get_queryset()
		names = ""
		dates = ""
		for entry in data:
			names += entry.name+"\n"
			d = datetime.datetime.strptime(str(entry.created_date)[:10], '%Y-%m-%d')
			dates += d.strftime('%B %d %Y')+"\n"

		fields = [{"title":"Lists",
				   "value": names,
				   "short":True},
				  {"title":"Created On",
				   "value": dates,
				   "short":True}]

		return fields

	def NewList(self, request, base, attachments):
		'''
		Creates a new List by sending a POST request to the ListViewSet
		with the parsed name.
		'''
		name = " ".join(request.data["text"].split(":")[1:]).strip()
		post_data = {'name': name}
		#sends a POST request to the lists URL to add the new list
		response = requests.post(request.build_absolute_uri()
								+'v2/lists/',data=post_data)
		#Grab the current lists into fields
		fields = self.ReturnLists()
		base['text'] = "Here are the current lists\n"
		attachments['fields'] = fields
		base['attachments'] = [attachments]
		return base

	def DisplayLists(self, base, attachments):
		'''
		Uses the fields grabbed by the ReturnLists function and returns
		the correctly formatted JSON 
		'''
		#Grab the current lists into fields
		fields = self.ReturnLists()
		base['text'] = "Here are the current lists\n"
		#attach fields into attachments
		attachments['fields'] = fields
		base['attachments'] = [attachments]
		return base

	def RemoveList(self, request, base, attachments):
		'''
		Remove the specified list object iff the passed in List name exists
		'''
		removeerror_texts = ["Um can't remove lists that don't exist.",
							 "Did you spell the list's name correctly?",
							 "Couldn't find the list"]
		to_remove = " ".join(request.data["text"].split(":")[1:]).strip()
		#if there is a List object with the specified list name, delete it
		if models.List.objects.filter(name=to_remove):
			base['text'] = "Sayonara {}".format(to_remove)
			models.List.objects.filter(name=to_remove).delete()
		else:
			base['text'] = random.choice(removeerror_texts)
		return base

	def LoveResponse(base, attachments):
		'''
		idk gifs I guess
		'''
		love_gifs = ["https://media.giphy.com/media/l2JI48S4tmnG4qi7m/giphy.gif",
					 "https://media.giphy.com/media/vuZGjpWJvIaVq/giphy.gif",
					 "https://media.giphy.com/media/xT5LMLMPdRh2VRNVLi/giphy.gif",
					]
		attachments['image_url'] = random.choice(love_gifs)
		base['attachments'] = [attachments]
		return base

	def CommandNotRecognized(self, base, attachments):
		'''
		throws you a gif or a reply if your command couldn't be parsed
		'''
		error_texts = ["NO ENTIENDO","?????","I DON'T UNDERSTAND YOU","You what?"]
		error_gifs = ["https://media.giphy.com/media/kaq6GnxDlJaBq/giphy.gif",
					  "https://media.giphy.com/media/v0eHX3n28wvoQ/giphy.gif",
					  "https://media.giphy.com/media/91fEJqgdsnu4E/giphy.gif"]
		gif_or_text = ["gif", "text"]
		choice = random.choice(gif_or_text)
		if choice == "gif":
			attachments['image_url'] = random.choice(error_gifs)
		else:
			base['text'] = random.choice(error_texts)
		return base

	def ListDetails(self, base, attachments, name):
		'''
		Returns all the entries in a specified List as seperate attachments.
		Each attachment has an Upvote, Add Comment, and Delete Entry button.
		A View Comment is displayed if the entry has comments.

		'''
		error_texts = ["Um can't remove lists that don't exist.",
					   "Did you spell the list's name correctly?",
					   "Couldn't find the list"]
		
		r = lambda: random.randint(0,255)
		combined_attachments = []
		attachments = None
		if name in self.ListIndex:
			entries = models.Entry.objects.filter(parent=self.ListIndex[name])
			if entries:
				for entry in entries:
					comments = models.Comment.objects.filter(parent=entry.id)
					new_entry = {}
					#Base for delete button
					delete = {"name":"delete",
							  "text":"Delete Entry",
							  "type":"button",
							  "value":"delete",
							  "style": "danger",
							  "confirm":{
							  	"title": "Are you sure?",
							    "text": "This literally kills the entry for good",
							    "ok_text": "Yes",
							    "dismiss_text": "No"
							  	}
							  }
					#Base for upvote button
					upvote = {"name":"upvote",
							  "text":"Upvote",
							  "type":"button",
							  "style": "primary",
							  "value":"upvote",
							  }
					#Base for add comment button
					makecomment = {"name":"comment",
							   "text":"Add Comment",
							   "type":"button",
							   "value":"comment",
							  	  }
					if len(comments)>0:
						#Base for view comments button
						viewcomment = {"name":"comment",
									   "text":"View Comments",
									   "type":"button",
									   "value":"comment",
							  		  }
						new_entry["actions"] = [upvote, makecomment, delete, viewcomment]
					else:
						new_entry["actions"] = [upvote, makecomment, delete]
					d = datetime.datetime.strptime(str(entry.created_at)[:10], '%Y-%m-%d')
					date = d.strftime('%B %d %Y')
					new_entry["pretext"] = "`Upvotes: {}`\t`By: {}`\t`On: {}`\t`Comments: {}`".format(
										entry.upvotes, entry.username, date, len(comments))
					new_entry["text"] = entry.entrydescription
					new_entry["color"] = '#%02X%02X%02X' % (r(),r(),r())
					new_entry["mrkdwn_in"] = ["text", "pretext"]
					new_entry["callback_id"] = entry.id
					new_entry["actions"] = [upvote, makecomment, delete]
					combined_attachments.append(new_entry)
				base['attachments'] = combined_attachments
				base['text'] = "*Entries in {}*".format(name)
			else:
				base['text'] = ("*No Entries in this list.*" +
								"Add one with `/lists {} add:`".format(name))
		else:
			base['text'] = random.choice(error_texts)
		return base

	def AddEntry(self, request, base, attachments):
		'''
		Add a new entry into a specified List if the List exists.
		'''
		data = request.data
		#parse the request data
		inputtext = data["text"].split("add:")
		ListName = inputtext[0].strip()
		EntryText = inputtext[1].strip()
		#if the list typed is valid
		if ListName in self.ListIndex:
			post_data = {'parent': self.ListIndex[ListName],
						 'entrydescription': EntryText,
						 'username':data["user_name"]}
			#sends a POST request to the Entry URL to add a new Entry in specified PK
			response = requests.post(request.build_absolute_uri()
								+'v2/entries/',data=post_data)
			return self.ListDetails(base, attachments, ListName)
		else:
			base['text'] = "Specified list doesn't exist"
		return base

	def AddComment(self, request, base, attachments):
		'''
		Add a new comment into a specified Entry if the entry exists
		'''
		data = request.data
		#parse the request data
		inputtext = data["text"].split("comment:")[1].strip().split(",")
		EntryID = inputtext[0].strip()
		CommentText = inputtext[1].strip()
		#if the list typed is valid
		print(EntryID,CommentText)
		if models.Entry.objects.filter(id=EntryID):
			print("found")
			post_data = {'parent': EntryID,
						 'comment': CommentText,
						 'name':data["user_name"]}
			#sends a POST request to the Entry URL to add a new Entry in specified PK
			response = requests.post(request.build_absolute_uri()
								+'v2/comments/',data=post_data)
			return self.CommentDetails(base, attachments, EntryID)
		else:
			base['text'] = "Specified entry doesn't exist. Can't add comment. soorrrry"
		return base

	def CommentDetails(self, base, attachments, EntryID):
		comments = models.Comment.objects.filter(parent=EntryID)
		r = lambda: random.randint(0,255)
		combined_attachments = []
		attachments = None
		if comments:
			for comment in comments:
				new_comment = {}
				delete = {"name":"deletecomment",
						  "text":"Delete Comment",
						  "type":"button",
						  "value":"delete",
						  "style": "danger",
						  "confirm":{
						  	"title": "Are you sure?",
						    "text": "This literally kills the entry for good",
						    "ok_text": "Yes",
						    "dismiss_text": "No"
						  	}
						  }
				d = datetime.datetime.strptime(str(comment.created_date)[:10], '%Y-%m-%d')
				date = d.strftime('%B %d %Y')
				new_comment["text"] = comment.comment
				new_comment["color"] = '#%02X%02X%02X' % (r(),r(),r())
				new_comment["pretext"] = "`By: {}`\t`On: {}`\t".format(comment.name, date)
				new_comment["mrkdwn_in"] = ["text", "pretext"]
				new_comment["callback_id"] = comment.id
				new_comment["actions"] = [delete]
				combined_attachments.append(new_comment)
			base['attachments'] = combined_attachments
			base['text'] = "*Comments about {}*".format(models.Entry.objects.filter(id=EntryID)[0])
		return base

def AuthorizeSlack(request):
	client_id = settings.SLACK_CLIENT_ID
	return render(request, 'landing.html', {'client_id':client_id})

class ButtonResponse(APIView):
	def post(self, request):
		text = ""
		appl={
  		"response_type": "in_channel",
  		"replace_original": True,
  		"text": text
		}
		attachments = {"callback_id": "list","color": "#3AA3E3",
					"attachment_type": "default", "mrkdwn_in": ["text", "pretext"] }
		data = json.loads(str(request.data["payload"]))
		callback_id = data['callback_id']
		action = data['actions'][0]['name']
		url = data['response_url']
		headers = {'Content-type': 'application/json', 'Accept': 'text/plain'}
		got = models.Entry.objects.filter(id=callback_id)
		if action == "delete":
			text = ('Deleted "{}"'.format(got[0].entrydescription))
			got[0].delete()
		elif action == "upvote":
			text = ("Upvoted {}".format(got[0].entrydescription))
			current_vote = got[0].upvotes
			got.update(upvotes=current_vote+1)
		elif action == "comment":
			text = ("Type: /lists comment:{}, -your comment here-".format(callback_id))
		elif action == "deletecomment":
			got = models.Entry.objects.filter(id=callback_id)
			text = ('Deleted "{}"'.format(got[0].comment))
			got[0].delete()

		requests.post(url,data=json.dumps(appl), headers=headers)

		return HttpResponse(status=204)

	def sendMessageToSlackResponseURL(responseURL, message):
		postOptions = {'uri':responseURL, method: 'POST', heads: {'Content-type':'application/json'},
					   'json':JSONmessage}

	def ListDetails(self, base, attachments, name):
		'''
		Returns all the entries in a specified List as seperate attachments.
		Each attachment has an Upvote, Add Comment, and Delete Entry button.
		A View Comment is displayed if the entry has comments.

		'''
		error_texts = ["Um can't remove lists that don't exist.",
					   "Did you spell the list's name correctly?",
					   "Couldn't find the list"]
		
		r = lambda: random.randint(0,255)
		combined_attachments = []
		attachments = None
		if name in self.ListIndex:
			entries = models.Entry.objects.filter(parent=self.ListIndex[name])
			if entries:
				for entry in entries:
					comments = models.Comment.objects.filter(parent=entry.id)
					new_entry = {}
					#Base for delete button
					delete = {"name":"delete",
							  "text":"Delete Entry",
							  "type":"button",
							  "value":"delete",
							  "style": "danger",
							  "confirm":{
							  	"title": "Are you sure?",
							    "text": "This literally kills the entry for good",
							    "ok_text": "Yes",
							    "dismiss_text": "No"
							  	}
							  }
					#Base for upvote button
					upvote = {"name":"upvote",
							  "text":"Upvote",
							  "type":"button",
							  "style": "primary",
							  "value":"upvote",
							  }
					#Base for add comment button
					makecomment = {"name":"comment",
							   "text":"Add Comment",
							   "type":"button",
							   "value":"comment",
							  	  }
					if len(comments)>0:
						#Base for view comments button
						viewcomment = {"name":"comment",
									   "text":"View Comments",
									   "type":"button",
									   "value":"comment",
							  		  }
						new_entry["actions"] = [upvote, makecomment, delete, viewcomment]
					else:
						new_entry["actions"] = [upvote, makecomment, delete]
					d = datetime.datetime.strptime(str(entry.created_at)[:10], '%Y-%m-%d')
					date = d.strftime('%B %d %Y')
					new_entry["pretext"] = "`Upvotes: {}`\t`By: {}`\t`On: {}`\t`Comments: {}`".format(
										entry.upvotes, entry.username, date, len(comments))
					new_entry["text"] = entry.entrydescription
					new_entry["color"] = '#%02X%02X%02X' % (r(),r(),r())
					new_entry["mrkdwn_in"] = ["text", "pretext"]
					new_entry["callback_id"] = entry.id
					new_entry["actions"] = [upvote, makecomment, delete]
					combined_attachments.append(new_entry)
				base['attachments'] = combined_attachments
				base['text'] = "*Entries in {}*".format(name)
			else:
				base['text'] = ("*No Entries in this list.*" +
								"Add one with `/lists {} add:`".format(name))
		else:
			base['text'] = random.choice(error_texts)
		return base

	def CommentDetails(self, base, attachments, EntryID):
		comments = models.Comment.objects.filter(parent=EntryID)
		r = lambda: random.randint(0,255)
		combined_attachments = []
		attachments = None
		if comments:
			for comment in comments:
				new_comment = {}
				delete = {"name":"deletecomment",
						  "text":"Delete Comment",
						  "type":"button",
						  "value":"delete",
						  "style": "danger",
						  "confirm":{
						  	"title": "Are you sure?",
						    "text": "This literally kills the entry for good",
						    "ok_text": "Yes",
						    "dismiss_text": "No"
						  	}
						  }
				d = datetime.datetime.strptime(str(comment.created_date)[:10], '%Y-%m-%d')
				date = d.strftime('%B %d %Y')
				new_comment["text"] = comment.comment
				new_comment["color"] = '#%02X%02X%02X' % (r(),r(),r())
				new_comment["pretext"] = "`By: {}`\t`On: {}`\t".format(comment.name, date)
				new_comment["mrkdwn_in"] = ["text", "pretext"]
				new_comment["callback_id"] = comment.id
				new_comment["actions"] = [delete]
				combined_attachments.append(new_comment)
			base['attachments'] = combined_attachments
			base['text'] = "*Comments about {}*".format(models.Entry.objects.filter(id=EntryID)[0])
		return base

#@permission_classes((IsAuthenticated, ))
class ListViewSet(viewsets.ModelViewSet):
	'''
	Spooky ghost magic. Specify the queryset and serializer to use and it creates
	the entire API that can GET, POST, PUT, and delete. 
	'''
	queryset = models.List.objects.all()
	serializer_class = serializers.ListSerializer
	#if a get request happens with the url field 'entries' after specific list.
	#query for the list object, then query for every entry child of the object.
	@detail_route(methods=['get'])
	def entries(self, request, pk=None):
		theList = self.get_object()
		serializer = serializers.EntrySerializer(
			theList.entries.all(), many=True)
		return Response(serializer.data)


#@permission_classes((IsAuthenticated, ))
class EntryViewSet(mixins.CreateModelMixin,
				   mixins.RetrieveModelMixin,
				   mixins.DestroyModelMixin,
				   mixins.UpdateModelMixin,
				   viewsets.GenericViewSet):
	'''
	Exactly same inheritence as above but deleted ListModelMixin.
	Inserted so you can't query for all entries and must specify what 
	the primary key of the entry is.
	'''
	queryset = models.Entry.objects.all()
	serializer_class = serializers.EntrySerializer

#@permission_classes((IsAuthenticated, ))
class CommentViewSet(mixins.CreateModelMixin,
				   mixins.RetrieveModelMixin,
				   mixins.DestroyModelMixin,
				   mixins.UpdateModelMixin,
				   viewsets.GenericViewSet):
	queryset = models.Comment.objects.all()
	serializer_class = serializers.CommentSerializer
