from rest_framework import serializers
from . import models

class ListSerializer(serializers.ModelSerializer):
	#List all entries associated with each list as hyperlinks, rather than full content.
	entries = serializers.HyperlinkedRelatedField(many=True,
												  read_only=True,
												  view_name='apiv2:entry-detail')

	entry_count = serializers.SerializerMethodField()

	class Meta:
		fields = (
			'name',
			'created_date',
			'id',
			'entries',
			'entry_count',
			)
		id = serializers.ReadOnlyField()
		model = models.List

	def get_entry_count(self, obj):
		return obj.entries.count()

class EntrySerializer(serializers.ModelSerializer):
	comments = serializers.HyperlinkedRelatedField(many=True,
												  read_only=True,
												  view_name='apiv2:comment-detail')

	class Meta:
		fields = (
			'id',
			'parent',
			'entrydescription',
			'created_at',
			'username',
			'comments',
			)
		parent = serializers.SlugRelatedField(queryset=models.List.objects.all(), slug_field="list_pk")
		model = models.Entry

	def validate_contents(self, value):
		'''
		Check to see entry text is actually substatial enough to warrant inclusion
		'''
		if len(value) < 1:
			raise serializers.ValidationError("Let's be a bit more descriptive about the project.")
		return value
		
class CommentSerializer(serializers.ModelSerializer):
	class Meta:
		fields = (
			'id',
			'parent',
			'comment',
			'created_date',
			'name',
			)
		parent = serializers.SlugRelatedField(queryset=models.List.objects.all(), slug_field="entry_pk")
		model = models.Comment

	def validate_comment(self, value):
		'''
		Check to see entry text is actually substatial enough to warrant inclusion
		'''
		if len(value) < 1:
			raise serializers.ValidationError("Let's be a bit more descriptive with the comment.")
		return value