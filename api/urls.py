from django.conf.urls import url, include
from rest_framework import routers

from . import views

#Create router instance
router = routers.SimpleRouter()
#specify that all the views in the List set to start with url "lists"
router.register(r'lists', views.ListViewSet)
#specify that all the views in the List set to start with url "entries"
router.register(r'entries', views.EntryViewSet)
#specify that all the views in the List set to start with url "comment"
router.register(r'comments', views.CommentViewSet)
#test route

urlpatterns = [
	url(r'^$', views.test.as_view()),
    url(r'^v2/', include(router.urls, namespace='apiv2')),
	url(r'^buttons/', views.ButtonResponse.as_view()),
]	

