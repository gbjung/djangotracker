TrackerBot is an experimental, re-usable "list maker" that has potential for tracking IT R&D ideas, bugs/issues we find with our SaaS tools, or cyber security events and actions taken—all within Slack. You can store entries in a list, upvote them, and comment on them. Interactions happen either via Slash Commands or Bot conversations (as if it were a real user).

TrackBot lives in a seperate Web API built with Django in which Slack interacts with via Slash Commands in the TrackBot App. All Slash commands are post requests and returns a JSON response for Slack to interpret. 

All requirements for running locally are listed in the requirements.txt


Current Commands:
/list lists		<--- Displays all lists
/list new:		<--- Creates a new list 
/list remove:		<--- Removes a specified list based on the list's name

In Progress:
/list show:		<--- Show all entries in a specified list
/entry 			<--- Slash commands to add/remove/upvote entries to specified lists
/comment		<--- Slash commands to add/remove comments to specified entries
More interactability through buttons