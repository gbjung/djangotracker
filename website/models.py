from __future__ import unicode_literals
from django.contrib.auth.models import User
from django.db import models

# Create your models here.

class SlackAuth(models.Model):
	user = models.ForeignKey(User, on_delete=models.CASCADE)
	access_token = models.CharField(max_length=255)
	team_id = models.CharField(max_length=255)
	name = models.CharField(max_length=50)
	slack_id = models.CharField(max_length=50)
	avatar = models.CharField(max_length=255)
	email = models.EmailField(max_length=254)
