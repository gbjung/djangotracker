from django.shortcuts import render, redirect
from django.core.urlresolvers import reverse_lazy
from django.views.generic import (TemplateView, ListView, DetailView,
								  CreateView, UpdateView, DeleteView,
								  FormView)
from django.views.generic.edit import FormMixin
from django.contrib.auth.mixins import LoginRequiredMixin
from django.shortcuts import get_object_or_404
from django.conf import settings
from django.contrib.auth.models import User
from django.contrib.auth import authenticate, login
from django.contrib import messages
import requests
import json
import random
from models import SlackAuth
from api import models
from . import forms
# Create your views here.


def SlackAuthorization(request):
	token = request.GET['code']
	r = requests.get('https://slack.com/api/oauth.access?client_id={}&client_secret={}&code={}'.format(
		settings.SLACK_CLIENT_ID, settings.SLACK_CLIENT_SECRET, token)).json()
	access_token = r['access_token']
	team_id = r['team']['id']
	if team_id == settings.TEAM_ID:
		slackuser = r['user']
		slack_id = slackuser['id']
		name = slackuser['name']
		email = slackuser['email']
		avatar = slackuser['image_1024'].replace('\\','')
		user, created = User.objects.get_or_create(username=email)
		if created:
			user.set_password(access_token)
			user.save()
			SlackAuth.objects.create(user=user, access_token=access_token,
								  team_id=team_id, name=name, slack_id=slack_id,
								  avatar=avatar, email=email)
			user.save()
		login(request, user)
		return redirect('website:list')
	else:
		messages.add_message(request, messages.INFO, "You must be a part of the BluestoneLogic")
		return redirect('website:home')

class HomeView(TemplateView):
	template_name = 'index.html'

	def get_context_data(self, **kwargs):
		context = super(HomeView, self).get_context_data(**kwargs)
		if messages.get_messages(self.request):
			context["message"] = messages.get_messages(self.request)
		return context

	# def dispatch(self, request, *args, **kwargs):
 #         if self.request.user.is_authenticated():
 #         	return redirect('website:list')
 #         return super(HomeView, self).dispatch(request, *args, **kwargs)
'''
Generic CRUD for lists
'''
class ListListView(LoginRequiredMixin,FormMixin, ListView):
	model = models.List
	context_object_name = "lists"
	template_name = "list_lists.html"
	login_url = reverse_lazy('website:home')
	form_class = forms.ListForm

	def get_success_url(self):
		return reverse_lazy('website:list')

	def get_context_data(self, **kwargs):
		context = super(ListListView, self).get_context_data(**kwargs)
		context['form'] = self.get_form()
		return context

	def post(self, request, *args, **kwargs):
		if not request.user.is_authenticated():
			return HttpResponseForbidden()
		form = self.get_form()
		if form.is_valid():
			return self.form_valid(form)
		else:
			return self.form_invalid(form)

	def form_valid(self, form):
		f = form.save(commit=False)
		f.creator_id = SlackAuth.objects.filter(user=self.request.user)[0].pk
		f.save()
		return super(ListListView, self).form_valid(form)

class ListDetailView(LoginRequiredMixin, DetailView, FormMixin):
	model = models.List
	template_name = "list_detail.html"
	form_class = forms.EntryForm

	def get_success_url(self):
		return reverse_lazy('website:listdetail', kwargs={'pk':self.object.pk})

	def get_context_data(self, **kwargs):
		context = super(ListDetailView, self).get_context_data(**kwargs)
		entries = models.Entry.objects.filter(parent=context['list'])
		context['entries'] = {}
		context['curruser'] = SlackAuth.objects.filter(user=self.request.user)[0]
		for entry in entries:
			context['entries'][entry] = {}
			votes = models.Vote.objects.filter(parententry=entry)
			upvotes = votes.filter(vote="UPVOTE")
			downvotes = votes.filter(vote="DOWNVOTE")
			context['entries'][entry]["upvotes"] = len(upvotes)
			context['entries'][entry]["downvotes"] = len(downvotes)
		context['form'] = self.get_form()
		return context

	def post(self, request, *args, **kwargs):
		if not request.user.is_authenticated():
			return HttpResponseForbidden()
		self.object = self.get_object()
		form = self.get_form()
		if form.is_valid():
			return self.form_valid(form)
		else:
			return self.form_invalid(form)

	def form_valid(self, form):
		parent = get_object_or_404(models.List, pk=self.object.pk)
		f = form.save(commit=False)
		f.username = SlackAuth.objects.filter(user=self.request.user)[0].name
		f.parent = parent
		f.save()
		return super(ListDetailView, self).form_valid(form)

def UpdateVote(request):
	print request
	return reverse('website:list')

class ListCreateView(LoginRequiredMixin, CreateView):
	fields = ("name",)
	model = models.List
	template_name = "list_form.html"

	def form_valid(self, form):
		f = form.save(commit=False)
		f.creator = SlackAuth.objects.filter(user=self.request.user)[0]
		f.save()
		return super(ListCreateView, self).form_valid(form)

class ListUpdateView(LoginRequiredMixin, UpdateView):
	fields = ("name",)
	model = models.List
	template_name = "list_form.html"

class ListDeleteView(LoginRequiredMixin, DeleteView):
	model=models.List
	success_url = reverse_lazy("website:list")
	template_name = "list_confirm_delete.html"

	def get_queryset(self):
		if not self.request.user.is_superuser:
			return self.model.objects.filter(creator=SlackAuth.objects.filter(user=self.request.user)[0])
		return self.model.objects.all()

class EntryDetailView(LoginRequiredMixin, DetailView, FormMixin):
	model = models.Entry
	template_name = "entry_detail.html"
	form_class = forms.CommentForm

	def get_context_data(self, **kwargs):
		context = super(EntryDetailView, self).get_context_data(**kwargs)
		context['comments'] = models.Comment.objects.filter(parent=context['entry'])
		context['form'] = self.get_form()
		return context

	def get_success_url(self):
		return reverse_lazy('website:entrydetail', kwargs={'pk':self.object.pk})


	def post(self, request, *args, **kwargs):
		if not request.user.is_authenticated():
			return HttpResponseForbidden()
		self.object = self.get_object()
		form = self.get_form()
		if form.is_valid():
			return self.form_valid(form)
		else:
			return self.form_invalid(form)

	def form_valid(self, form):
		parent = get_object_or_404(models.Entry, pk=self.object.pk)
		f = form.save(commit=False)
		f.name = SlackAuth.objects.filter(user=self.request.user)[0].name
		f.parent = parent
		f.save()
		return super(EntryDetailView, self).form_valid(form)

class EntryCreateView(LoginRequiredMixin, CreateView):
	fields = ("entrydescription","username")
	model = models.Entry
	template_name = "entry_form.html"

	def get_initial(self):
		inital = super(EntryCreateView, self).get_initial(self)
		inital["username"] = SlackAuth.objects.filter(user=self.request.user)[0].name

class EntryUpdateView(LoginRequiredMixin, UpdateView):
	fields = ("entrydescription",)
	model = models.Entry
	template_name = "entry_form.html"

class EntryDeleteView(LoginRequiredMixin, DeleteView):
	model = models.Entry
	template_name = "entry_confirm_delete.html"

	def get_success_url(self):
		return reverse_lazy('website:listdetail', args = (self.object.parent.pk,))
