from api import models
from django import forms

class EntryForm(forms.ModelForm):
	
	class Meta:
		model = models.Entry
		fields = ('entrydescription', )
		
	def __init__(self, *args, **kwargs):
		super(EntryForm,self).__init__(*args, **kwargs)
		self.fields['entrydescription'].widget = forms.TextInput(attrs={
			'class': 'textfield',
			'placeholder': 'Add a new entry '
			})

class CommentForm(forms.ModelForm):
	
	class Meta:
		model = models.Comment
		fields = ('comment',)

class ListForm(forms.ModelForm):
	name = forms.CharField( label='')
	class Meta:
		model = models.List
		fields = ('name',)