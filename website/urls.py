from django.conf.urls import url, include
from . import views


urlpatterns = [
	url(r'^login/$', views.SlackAuthorization, name='login'),
	url(r'^$', views.HomeView.as_view(), name='home'),
	url(r'^lists/$', views.ListListView.as_view(), name='list'),
	url(r'^lists/(?P<pk>\d+)/$', views.ListDetailView.as_view(), name='listdetail'),
	url(r'^lists/create/$', views.ListCreateView.as_view(), name='createlist'),
	url(r'^lists/edit/(?P<pk>\d+)/$', views.ListUpdateView.as_view(), name='updatelist'),
	url(r'^lists/delete/(?P<pk>\d+)/$', views.ListDeleteView.as_view(), name='deletelist'),
	url(r'^entries/(?P<pk>\d+)/$', views.EntryDetailView.as_view(), name='entrydetail'),
	url(r'^entries/edit/(?P<pk>\d+)/$', views.EntryUpdateView.as_view(), name='updateentry'),
	url(r'^entries/delete/(?P<pk>\d+)/$', views.EntryDeleteView.as_view(), name='deleteentry'),
	url(r'^vote/(?P<user>\d+)/(?P<pk>\d+)/$', views.HomeView.as_view(), name="vote"),
]